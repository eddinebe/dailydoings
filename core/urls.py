from django.urls import path
from core import views


app_name = 'core'

urlpatterns = [
    path('', views.index, name='index'),
    path('doings/', views.doings, name='doings'),
    path('doings/<int:pk>/', views.doings, name='doings_pk'),
]
