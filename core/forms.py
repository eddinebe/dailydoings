from django.forms import ModelForm
from core.models import Doings


class DoingsForm(ModelForm):
    class Meta:
        model = Doings
        fields = ('text',)
