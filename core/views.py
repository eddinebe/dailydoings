from django.shortcuts import render, reverse, get_object_or_404
from core.models import Doings
from core.forms import DoingsForm
from django.http import HttpResponseRedirect


def index(request):
    return HttpResponseRedirect(reverse('core:doings'))


def doings(request, pk=None):
    doings_form = None

    if request.method == 'POST':
        doings_form = DoingsForm(request.POST)
        if doings_form.is_valid():
            doings_form.save()
            doings_form = None

    if request.method == 'DELETE':
        todo = get_object_or_404(Doings, pk=pk)
        todo.delete()

    if not doings_form:
        doings_form = DoingsForm()

    context = {'doings': Doings.objects.all(), 'doings_form': doings_form}

    return render(request, 'core/index.html', context)
